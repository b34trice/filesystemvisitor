﻿using System.Collections.Generic;

namespace FileSystemVisitor
{
    public class FolderEntityModel
    {
        public string FolderName { get; set; }
        public IEnumerable<FolderEntityModel> Folders { get; set; }
        public IEnumerable<string> Files { get; set; }

        public FolderEntityModel()
        {
            Folders = new List<FolderEntityModel>();
            Files = new List<string>();
        }
    }
}