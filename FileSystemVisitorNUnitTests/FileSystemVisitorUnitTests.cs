﻿using FileSystemVisitor;
using FileSystemVisitor.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace FileSystemVisitorNUnitTests
{
    [TestFixture]
    public class FileSystemVisitorUnitTests
    {
        private const string PATH = "C:\\temp";
        private Mock<IFolderService> _folderService;
        private Mock<IFilesService> _fileService;
        private FileSystemVisitorService _service;

        [SetUp]
        public void Setup()
        {
            _folderService = new Mock<IFolderService>();
            _folderService.Setup(e => e.GetFolders(It.IsAny<List<FolderEntityModel>>(), It.IsAny<Func<FolderEntityModel, bool>>())).Returns<List<FolderEntityModel>>(s => s);
            _fileService.Setup(e => e.GetFiles(It.IsAny<string>(), It.IsAny<Func<string, bool>>())).Returns<List<string>>(s => s);
            _fileService = new Mock<IFilesService>();

            _service = new FileSystemVisitorService(file => file.Contains(".csv"), folder => folder.FolderName.Contains("back"), _fileService.Object, _folderService.Object);
        }

        public class StartSearch : FileSystemVisitorUnitTests
        {
            [TestCase("C:\\temp", "C:\\temp\\epam.activities.exchange", "C:\\temp\\epam.activities.exchange\\1.8.6")]
            public void StartSearch_VerifyServicesCallsWithTempFolder(string tempFolderPath, string exchangeFolderPath, string versionFolderPath)
            {
                var actualResult = _service.StartSearch(PATH);

                _fileService.Verify(mock => mock.GetFiles(tempFolderPath, It.IsAny<Func<string, bool>>()), Times.Once());
                _fileService.Verify(mock => mock.GetFiles(exchangeFolderPath, It.IsAny<Func<string, bool>>()), Times.Once());
                _fileService.Verify(mock => mock.GetFiles(versionFolderPath, It.IsAny<Func<string, bool>>()), Times.Once());

                var tempModel1 = new List<FolderEntityModel>();
                var tempModel2 = new List<FolderEntityModel> { new FolderEntityModel { FolderName = exchangeFolderPath, Files = new List<string>(), Folders = new List<FolderEntityModel>() } };
                var tempModel3 = new List<FolderEntityModel> { new FolderEntityModel { FolderName = versionFolderPath, Files = new List<string>(), Folders = new List<FolderEntityModel>() } };
                var tempModel4 = new List<FolderEntityModel> { new FolderEntityModel { FolderName = $"{tempFolderPath}\\back", Files = new List<string>(), Folders = new List<FolderEntityModel>() } };

                _folderService.Verify(mock => mock.GetFolders(tempModel1, It.IsAny<Func<FolderEntityModel, bool>>()), Times.Exactly(2));
                _folderService.Verify(mock => mock.GetFolders(tempModel2, It.IsAny<Func<FolderEntityModel, bool>>()), Times.Once());
                _folderService.Verify(mock => mock.GetFolders(tempModel3, It.IsAny<Func<FolderEntityModel, bool>>()), Times.Once());
                _folderService.Verify(mock => mock.GetFolders(tempModel4, It.IsAny<Func<FolderEntityModel, bool>>()), Times.Once());
            }
        }
    }
}