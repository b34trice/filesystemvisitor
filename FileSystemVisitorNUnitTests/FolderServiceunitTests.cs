﻿using FileSystemVisitor;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace FileSystemVisitorNUnitTests
{
    [TestFixture]
    public class FolderServiceUnitTests
    {
        private const string TEMP_FOLDER_PATH = "C:\\temp";
        private FolderService _service;
        private Func<FolderEntityModel, bool> _filter;

        [SetUp]
        public void Setup()
        {
            _service = new FolderService();
            _filter = name => name.FolderName.Contains("back");
        }

        [TestFixture]
        public class GetFolders : FolderServiceUnitTests
        {
            [Test]
            public void GetFolder_GetResultWithValidPath()
            {
                var expectedResult = new List<FolderEntityModel>();

                var folders = new List<FolderEntityModel> { new FolderEntityModel { Files = new List<string>(), Folders = new List<FolderEntityModel>(), FolderName = TEMP_FOLDER_PATH } };
                var actualresult = _service.GetFolders(folders, _filter);
                 CollectionAssert.AreEqual(actualresult, expectedResult);
            }

            [TestCase("C:\\back")]
            public void GetFolder_GetResultWithInvalidPath(string invalidPath)
            {
                var expectedResult = new List<FolderEntityModel> { new FolderEntityModel { FolderName = TEMP_FOLDER_PATH, Folders = new List<FolderEntityModel>(), Files = new List<string>() } };

                var folders = new List<FolderEntityModel> { new FolderEntityModel { Files = new List<string>(), Folders = new List<FolderEntityModel>(), FolderName = invalidPath } };
                var actualresult = _service.GetFolders(folders, _filter);
                CollectionAssert.AreEqual(actualresult, expectedResult);
            }


            [Test]
            public void GetFolder_GetResultAfterRemoveElement()
            {
                var expectedResult = new List<FolderEntityModel>();
                _service.OnFolderFiltered += (s, e) => { _service.ExcludeFolder(e.Folder); };
                var folders = new List<FolderEntityModel> { new FolderEntityModel { FolderName = TEMP_FOLDER_PATH, Folders = new List<FolderEntityModel>(), Files = new List<string>() } };
                var actualresult = _service.GetFolders(folders, _filter);
                CollectionAssert.AreEqual(actualresult, expectedResult);
            }
        }
    }
}
