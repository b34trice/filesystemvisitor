﻿using FileSystemVisitor;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace FileSystemVisitorNUnitTests
{
    [TestFixture]
    public class FileServiceUnitTests
    {
        private FilesService _service;
        private Func<string, bool> _filter;
        private const string PATH = "C:\\temp";
        private const string PATH_WITHOUT_CSV = "C:\\";

        [SetUp]
        public void Setup()
        {
            _service = new FilesService();
            _filter = name => name.Contains(".csv");
        }

        [TestFixture]
        public class GetFiles : FileServiceUnitTests
        {
            [TestCase("C:\\temp\\Encoding Time.csv")]
            public void GetFile_GetResultWithValidPath(string expectedFile)
            {
                var expectedResult = new List<string> { expectedFile };
                var actualresult = _service.GetFiles(PATH, _filter);
                CollectionAssert.AreEqual(actualresult, expectedResult);
            }

            [Test]
            public void GetFile_GetResultWithInvalidPath()
            {
                var expectedResult = new List<string>();
                var actualresult = _service.GetFiles(PATH_WITHOUT_CSV, _filter);
                CollectionAssert.AreEqual(actualresult, expectedResult);
            }

            [Test]
            public void GetFile_GetResultAfterRemoveElement()
            {
                var expectedResult = new List<string>();
                _service.OnFileFiltered += (s, e) => { _service.ExcludeFile(e.FileName); };
                var actualresult = _service.GetFiles(PATH, _filter);
                CollectionAssert.AreEqual(actualresult, expectedResult);
            }
        }
    }
}