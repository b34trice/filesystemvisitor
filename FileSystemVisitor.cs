﻿using FileSystemVisitor.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace FileSystemVisitor
{
    public class FileSystemVisitorService : IFileSystemVisitor
    {
        public event EventHandler OnStart;
        public event EventHandler OnFinish;

        private readonly IFilesService _filesService;
        private readonly IFolderService _folderService;

        private readonly Func<string, bool> _fileFilter;
        private readonly Func<FolderEntityModel, bool> _folderFilter;

        private FolderEntityModel _result;
        private bool _isStop;

        public FileSystemVisitorService(
            Func<string, bool> fileFilter,
            Func<FolderEntityModel, bool> folderFilter,
            IFilesService filesService,
            IFolderService folderService)
        {
            _fileFilter = fileFilter;
            _folderFilter = folderFilter;

            _filesService = filesService;
            _folderService = folderService;

        }
        public FolderEntityModel StartSearch(string path)
        {
            OnStart?.Invoke(this, new EventArgs());
            foreach (var entity in GetDirectoryChildrens(path))
            {
                _result = entity;
            }
            OnFinish?.Invoke(this, new EventArgs());
            return _result;
        }

        public void Stop()
        {
            _isStop = true;
        }

        private IEnumerable<FolderEntityModel> GetDirectoryChildrens(string rootDirectoryPath)
        {
            if (_isStop)
            {
                yield return default;
            }

            var result = new FolderEntityModel
            {
                FolderName = rootDirectoryPath,
                Files = _filesService.GetFiles(rootDirectoryPath, _fileFilter)
            };

            var folders = Directory.GetDirectories(rootDirectoryPath);

            var rootFolderChildrens = new List<FolderEntityModel>();
            foreach (var childrenFolder in folders)
            {
                foreach (var entityFolder in GetDirectoryChildrens(childrenFolder))
                {
                    rootFolderChildrens.Add(entityFolder);
                }
            }

            result.Folders = _folderService.GetFolders(rootFolderChildrens, _folderFilter);
            yield return result;
        }
    }
}