﻿namespace FileSystemVisitor.Events
{
    public class FolderFindEventArgs
    {
        public readonly FolderEntityModel Folder;

        public FolderFindEventArgs(FolderEntityModel folder)
        {
            Folder = folder;
        }
    }
}
