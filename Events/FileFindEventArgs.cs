﻿namespace FileSystemVisitor.Events
{
    public class FileFindEventArgs
    {
        public readonly string FileName;

        public FileFindEventArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}