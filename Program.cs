﻿using FileSystemVisitor.Events;
using FileSystemVisitor.Interfaces;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;

namespace FileSystemVisitor
{
    class Program
    {
        private const int PATH_INDEX = 0;

        private static readonly FileSystemVisitorService _fileSystemVisitor;
        private static readonly IFilesService _filesService;
        private static readonly IFolderService _folderService;

        static Program()
        {
            _filesService = new FilesService();
            _folderService = new FolderService();

            _fileSystemVisitor = new FileSystemVisitorService(
                file => file.Contains(".csv"),
                folder => folder.FolderName.Contains("back"),
                _filesService,
                _folderService
                );
        }

        static void Main(string[] args)
        {
            SubscribeToEvents();
            var result = _fileSystemVisitor.StartSearch(GetPath(args));
            Console.WriteLine($"Result: \n {JsonConvert.SerializeObject(result)}");
            Console.Read();
        }

        private static void SubscribeToEvents()
        {
            _fileSystemVisitor.OnStart += OnSearchStart;
            _fileSystemVisitor.OnFinish += OnSearchEnd;
            _filesService.OnFileFind += OnFileFind;
            _filesService.OnFileFiltered += OnFileFiltered;
            _folderService.OnFolderFind += OnFolderFind;
            _folderService.OnFolderFiltered += OnFolderFiltered;
        }

        private static string GetPath(string[] args)
        {
            if (args.Any())
            {
                var path = args[PATH_INDEX];
                if (Directory.Exists(path))
                {
                    return path;
                }
                else
                {
                    throw new ArgumentException("Folder path is invalid");
                }
            }
            else
            {
                throw new ArgumentException("Folder path is missing");
            }
        }

        private static void OnSearchStart(object sender, EventArgs e)
        {
            Console.WriteLine("Search start");
        }

        private static void OnSearchEnd(object sender, EventArgs e)
        {
            Console.WriteLine("Search end");
        }

        private static void OnFileFind(object sender, FileFindEventArgs e)
        {
            Console.WriteLine($"File find - {e.FileName}");
        }

        private static void OnFileFiltered(object sender, FileFindEventArgs e)
        {
            Console.WriteLine($"File filtered - {e.FileName}");
            if (e.FileName == "C:\\temp\\Encoding Time.csv")
            {
                _filesService.ExcludeFile(e.FileName);
            }
        }

        private static void OnFolderFind(object sender, FolderFindEventArgs e)
        {
            Console.WriteLine($"Folder find - {e.Folder.FolderName}");
        }

        private static void OnFolderFiltered(object sender, FolderFindEventArgs e)
        {
            Console.WriteLine($"Folder filtered - {e.Folder.FolderName}");
        }
    }
}