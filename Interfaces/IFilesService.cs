﻿using FileSystemVisitor.Events;
using System;
using System.Collections.Generic;

namespace FileSystemVisitor.Interfaces
{
    public interface IFilesService
    {
        event EventHandler<FileFindEventArgs> OnFileFind;
        event EventHandler<FileFindEventArgs> OnFileFiltered;

        IEnumerable<string> GetFiles(string path, Func<string, bool> filter);
        void ExcludeFile(string file);
    }
}