﻿using System;

namespace FileSystemVisitor.Interfaces
{
    public interface IFileSystemVisitor
    {
        public event EventHandler OnStart;
        public event EventHandler OnFinish;

        FolderEntityModel StartSearch(string path);
    }
}