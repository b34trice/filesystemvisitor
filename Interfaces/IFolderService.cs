﻿using FileSystemVisitor.Events;
using System;
using System.Collections.Generic;

namespace FileSystemVisitor.Interfaces
{
    public interface IFolderService
    {
        public event EventHandler<FolderFindEventArgs> OnFolderFind;
        public event EventHandler<FolderFindEventArgs> OnFolderFiltered;

        IEnumerable<FolderEntityModel> GetFolders(IEnumerable<FolderEntityModel> folders, Func<FolderEntityModel, bool> filter);
        void ExcludeFolder(FolderEntityModel folder);
    }
}