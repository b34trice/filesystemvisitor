﻿using FileSystemVisitor.Events;
using FileSystemVisitor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FileSystemVisitor
{
    public class FolderService : IFolderService
    {
        public event EventHandler<FolderFindEventArgs> OnFolderFind;
        public event EventHandler<FolderFindEventArgs> OnFolderFiltered;

        private List<FolderEntityModel> _resultFolders;
        private List<FolderEntityModel> _foldersForExclude = new List<FolderEntityModel>();

        public IEnumerable<FolderEntityModel> GetFolders(IEnumerable<FolderEntityModel> folders, Func<FolderEntityModel, bool> filter)
        {
            foreach (var folder in folders)
            {
                OnFolderFind?.Invoke(this, new FolderFindEventArgs(folder));
            }

            _resultFolders = folders.Where(filter).ToList();

            foreach (var folder in _resultFolders)
            {
                OnFolderFiltered?.Invoke(this, new FolderFindEventArgs(folder));
            }

            foreach (var folder in _foldersForExclude
                .Where(folder => _resultFolders.Contains(folder))
                .Select(folder => folder))
            {
                _resultFolders.Remove(folder);
            }

            return _resultFolders;
        }

        public void ExcludeFolder(FolderEntityModel folder)
        {
            _foldersForExclude.Add(folder);
        }
    }
}