﻿using FileSystemVisitor.Events;
using FileSystemVisitor.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileSystemVisitor
{
    public class FilesService : IFilesService
    {
        public event EventHandler<FileFindEventArgs> OnFileFind;
        public event EventHandler<FileFindEventArgs> OnFileFiltered;

        private List<string> _resultFiles;
        private List<string> _filesForExclude = new List<string>();

        public IEnumerable<string> GetFiles(string path, Func<string, bool> filter)
        {
            var allFiles = Directory.GetFiles(path).ToList();
            foreach (var file in allFiles)
            {
                OnFileFind?.Invoke(this, new FileFindEventArgs(file));
            }

            _resultFiles = allFiles.Where(filter).ToList();
            foreach (var file in _resultFiles)
            {
                OnFileFiltered?.Invoke(this, new FileFindEventArgs(file));
            }

            foreach (var file in _filesForExclude
                .Where(file => _resultFiles.Contains(file))
                .Select(file => file))
            {
                _resultFiles.Remove(file);
            }

            return _resultFiles;
        }

        public void ExcludeFile(string file)
        {
            _filesForExclude.Add(file);
        }
    }
}